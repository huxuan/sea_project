package com.example.demo.service;

import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import com.example.demo.model.PowerData;

import java.util.List;

public interface DeviceService {


    Device getDeviceInfo(String mac);

    //Device getDeviceInfo(Integer id);

    void updateDeviceInfo(String deviceMac, String deviceName, Double power, Integer status);

    Device addDevice(String username, String mac, String deviceName, Double power, Integer status);

    void deleteDevice(String mac);

    Boolean isWorkNormal(Device device);

    Boolean isDeviceExist(Device device);

    void turnOffDevice(String mac);

    void turnOnDevice(String mac);

    void turnOffSensor(String mac);

    boolean setPowerData(String key, Double power, Long timestamp);

    List<PowerData> getOrderedPowerDataByMacInRange(String mac, Long startTime, Long endTime);

    List<PowerData> getOrderedPowerDataByMac(String mac);

    boolean deletePowerData(String mac);

    boolean deleteExpiredPowerData(String mac, Long untilTime);

    List<Device> getAllDevice();




}
