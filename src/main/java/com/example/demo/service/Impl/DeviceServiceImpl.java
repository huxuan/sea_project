package com.example.demo.service.Impl;

import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import com.example.demo.model.PowerData;
import com.example.demo.repository.DeviceRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.DeviceService;
import com.example.demo.service.IMqttSender;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private UserService userService;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    IMqttSender iMqttSender;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Device getDeviceInfo(String mac) {
        return deviceRepository.findByDeviceMac(mac);
    }

    @Override
    public void updateDeviceInfo(String deviceMac, String deviceName, Double power, Integer status) {
        Date date = new Date();
        Long heartTime = date.getTime();
        deviceRepository.updateDeviceInfoByMac(deviceMac, deviceName, power, heartTime, status);
    }

    /*@Override
    public Device addDevice(Device device, User user) {
        device.setUser(user);
        return deviceRepository.save(device);
    }*/

    public Device addDevice(String username, String mac, String deviceName, Double power, Integer status) {
        Date date = new Date();
        Long timeStamp = date.getTime();
        Device device = new Device();
        device.setDeviceMac(mac);
        device.setDeviceName(deviceName);
        device.setPower(power);
        device.setHeartTime(timeStamp);
        device.setStatus(status);
        deviceRepository.save(device);
        User user = userService.getUser(username);
        Set<Device> deviceList = user.getDeviceList();
        deviceList.add(device);
        userService.saveUser(user);
        return device;
    }

    @Override
    public void deleteDevice(String mac) {
        deviceRepository.deleteByDeviceMac(mac);
    }

    /*@Override
    public void deleteDevice(Device device) {
        deviceRepository.delete(device);
    }*/

    @Override
    public Boolean isWorkNormal(Device device) {
        if (device.getPower()>Device.THRESHOLD){
            return false;
        }
        return true;
    }

    @Override
    public Boolean isDeviceExist(Device device) {
        if (deviceRepository.findByDeviceMac(device.getDeviceMac())!=null){
            return true;
        }
        return false;
    }

    @Override
    public void turnOffDevice(String mac) {
        iMqttSender.sendToMqtt("turn Off device "+ mac, "device_switch");
        deviceRepository.updateDeviceStatus(mac, Device.DEVICE_OFF);
        log.info("send command to turn off device "+ mac);
    }

    @Override
    public void turnOffSensor(String mac) {
        iMqttSender.sendToMqtt("turn Off Sensor "+ mac, "sensor_switch");
        log.info("send command to turn off Sensor "+ mac);
    }

    @Override
    public void turnOnDevice(String mac) {
        iMqttSender.sendToMqtt("turn On device "+ mac, "device_switch");
        deviceRepository.updateDeviceStatus(mac, Device.DEVICE_ON);
        log.info("send command to turn on device");

    }

    @Override
    public boolean setPowerData(String key, Double power, Long timestamp) {
        boolean result = false;
        try {
            redisTemplate.opsForZSet().add(key, String.valueOf(power), timestamp);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.toString());
        }
        return result;
    }

    @Override
    public List<PowerData> getOrderedPowerDataByMacInRange(String mac, Long startTime, Long endTime) {
        List<PowerData> powerDataList = new ArrayList<>();
        Set<ZSetOperations.TypedTuple<String>> dataSet = redisTemplate.opsForZSet().rangeByScoreWithScores(mac, startTime, endTime);
        Iterator<ZSetOperations.TypedTuple<String>> iterator = dataSet.iterator();
        while (iterator.hasNext()) {
            ZSetOperations.TypedTuple<String> dataTuple = iterator.next();
            PowerData powerData = new PowerData(dataTuple.getValue(),dataTuple.getScore().longValue());
            powerDataList.add(powerData);
        }
        log.info("get Ordered PowerData By Mac In Range:" + powerDataList);
        return powerDataList;
    }

    @Override
    public List<PowerData> getOrderedPowerDataByMac(String mac) {
        List<PowerData> powerDataList = new ArrayList<>();
        Set<ZSetOperations.TypedTuple<String>> dataSet = redisTemplate.opsForZSet().rangeWithScores(mac, 0, -1);
        Iterator<ZSetOperations.TypedTuple<String>> iterator = dataSet.iterator();
        while (iterator.hasNext()) {
            ZSetOperations.TypedTuple<String> dataTuple = iterator.next();
            PowerData powerData = new PowerData(dataTuple.getValue(),dataTuple.getScore().longValue());
            powerDataList.add(powerData);
        }
        log.info("get Ordered PowerData By Mac:" + powerDataList);
        return powerDataList;
    }

    @Override
    public boolean deletePowerData(String mac) {
        boolean result = false;
        try {
            redisTemplate.opsForZSet().removeRange(mac, 0,-1);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.toString());
        }
        return result;
    }

    @Override
    public boolean deleteExpiredPowerData(String mac, Long untilTime) {
        boolean result = false;
        try {
            redisTemplate.opsForZSet().removeRangeByScore(mac,0, untilTime);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.toString());
        }
        return result;
    }

    @Override
    public List<Device> getAllDevice() {
        return deviceRepository.findAll();
    }

}
