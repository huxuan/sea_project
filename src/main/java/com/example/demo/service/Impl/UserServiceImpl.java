package com.example.demo.service.Impl;

import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User register(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setRole("ROLE_USER");
        return userRepository.save(user);
    }


    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Boolean updatePassword(String username, String oldPassword, String newPassword) {

        User user = userRepository.findByUsername(username);
        if(user.getPassword().equals(bCryptPasswordEncoder.encode(oldPassword))){
            userRepository.updatePasswordByUsername(username,bCryptPasswordEncoder.encode(newPassword));
            return true;
        }else{
            log.info("user" + username +"not exist");
            return false;
        }
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

}
