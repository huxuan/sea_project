package com.example.demo.service;

import com.example.demo.entity.Device;
import com.example.demo.entity.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {

    User register(String username, String password);

    User getUser(String username);

    Boolean updatePassword(String username, String oldPassword, String newPassword);

    void saveUser(User user);


}
