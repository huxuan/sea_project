package com.example.demo.config;


import com.example.demo.entity.Device;
import com.example.demo.service.DeviceService;
import com.example.demo.service.IMqttSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.io.IOException;
import java.util.Date;

@Slf4j
@Configuration
@IntegrationComponentScan
public class MqttReceiverConfig {

    @Autowired
    DeviceService deviceService;

    @Autowired
    IMqttSender iMqttSender;

    @Value("${spring.mqtt.username}")
    private String username;

    @Value("${spring.mqtt.password}")
    private String password;

    @Value("${spring.mqtt.url}")
    private String hostUrl;

    @Value("${spring.mqtt.client.id}")
    private String clientId;

    @Value("${spring.mqtt.completionTimeout}")
    private int completionTimeout ;

    @Value("${spring.mqtt.receiver.default.topic}")
    private String receiverTopic;

    @Bean
    public MqttConnectOptions getReceiverMqttConnectOptions(){
        MqttConnectOptions options=new MqttConnectOptions();
        options.setUserName(username);
        options.setPassword(password.toCharArray());
        options.setServerURIs(new String[]{hostUrl});
        options.setKeepAliveInterval(20);
        return options;
    }

    @Bean
    public MqttPahoClientFactory receiverMqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getReceiverMqttConnectOptions());
        return factory;
    }

    //recevie channel
    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    //config mqtt client,listened topic
    @Bean
    public MessageProducer inbound() {
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter(clientId+"_inbound", receiverMqttClientFactory(),
                        receiverTopic);
        adapter.setCompletionTimeout(completionTimeout);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(1);
        //set subscribe channel
        adapter.setOutputChannel(mqttInputChannel());
        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                String topic = message.getHeaders().get("mqtt_receivedTopic").toString();
                String type = topic.substring(topic.lastIndexOf("/")+1, topic.length());
                if(receiverTopic.equalsIgnoreCase(topic)){
                   try {
                        Device device = new ObjectMapper().readValue(message.getPayload().toString(), Device.class);
                        Date date = new Date();
                        Long heartTime = date.getTime();
                        if(deviceService.isDeviceExist(device)){
                            deviceService.updateDeviceInfo(device.getDeviceMac(),device.getDeviceName(), device.getPower(), device.getStatus());
                            deviceService.setPowerData(device.getDeviceMac(), device.getPower(), heartTime);
                            if(!deviceService.isWorkNormal(device)){
                                String st = "Power of Device " + device.getDeviceName() +" is over the threshold";
                                iMqttSender.sendToMqtt(st, "alarm");
                            }
                        }else{
                            String st = "device "+device.getDeviceMac()+" not exist";
                            iMqttSender.sendToMqtt(st, "alarm");
                        }
                       log.info("receiver success,"+message.getPayload().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        log.info(e.toString());
                    }
                }
            }
        };
    }


}
