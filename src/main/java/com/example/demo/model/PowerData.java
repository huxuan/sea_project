package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PowerData implements Serializable {

    private static final long serialVersionUID = 662692455422902539L;

    private String power;

    private Long timestamp;
}