package com.example.demo.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;


@Entity
@Table(name="se_device")
@ApiModel("Device Object")
@AllArgsConstructor
@NoArgsConstructor
public class Device {

    public static final Integer DEVICE_ON= 1;
    public static final Integer DEVICE_OFF = 0;

    public static final Double THRESHOLD = 500.0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "deviceName")
    private String deviceName;

    @Column(name = "deviceMac" , nullable = false, unique = true)
    private  String deviceMac;

    @Column(name = "power")
    private Double power;

    @Column(name = "heartTime")
    private Long heartTime;

    @Column(name = "statu")
    private Integer status;

    /*@ApiModelProperty("user_id")
    @ManyToOne
    @JoinColumn(name="user_Id")
    private User user;*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceMac() {
        return deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    public Long getHeartTime() {
        return heartTime;
    }

    public void setHeartTime(Long heartTime) {
        this.heartTime = heartTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /*public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }*/
    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", deviceName='" + deviceName + '\'' +
                ", deviceMac='" + deviceMac + '\'' +
                ", power=" + power +
                ", heartTime=" + heartTime +
                ", status=" + status +
                '}';
    }


}
