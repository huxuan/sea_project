package com.example.demo.controller;


import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import com.example.demo.model.PowerData;
import com.example.demo.service.DeviceService;
import com.example.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.*;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

@RestController
@RequestMapping("/smartEnergyApp")
@Api(tags = "Device Interface")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(){
        return "home";
    }


    @ApiOperation("Get device list for one user by username")
    @RequestMapping(value = "/getDeviceList/{username}", method = RequestMethod.GET)
    public Set<Device> listDevice(@PathVariable("username") String username){
        User user = userService.getUser(username);
        Set<Device> deviceList = user.getDeviceList();
        return deviceList;
    }

    @ApiOperation("Get device detail for a specified device by device mac address")
    @RequestMapping(value = "/getDeviceInfo/{deviceMac}", method = RequestMethod.GET)
    public Device getDeviceInfo(@PathVariable("deviceMac") String mac){
        return deviceService.getDeviceInfo(mac);
    }


    @ApiOperation("add new device for a user for the first connection")
    @RequestMapping(value = "/addDevice", method = RequestMethod.PUT)
    public String addDevice(@RequestParam("username") String username,
                            @RequestParam("deviceMac") String deviceMac,
                            @RequestParam("deviceName") String deviceName,
                            @RequestParam("power") String power,
                            @RequestParam("status") String status){
        deviceService.addDevice(username,deviceMac,deviceName,parseDouble(power),parseInt(status));
        return "add success";
    }


    @ApiOperation("Update device detail")
    @RequestMapping(value = "/updateDevice", method = RequestMethod.POST)
    public String updateDevice(@RequestParam("deviceMac") String deviceMac,
                               @RequestParam("deviceName") String deviceName,
                               @RequestParam("power") String power,
                               @RequestParam("status") String status){
        deviceService.updateDeviceInfo(deviceMac,deviceName,parseDouble(power),parseInt(status));
        return "updateSuccess";
    }

    /*@ApiIgnore
    @RequestMapping(value = "/deleteDevice", method = RequestMethod.DELETE)
    public String deleteDevice(Device device){
        deviceService.deleteDevice(device);
        return "delete success";
    }*/

    @ApiOperation("Delete device by mac address")
    @RequestMapping(value = "/deleteDevice/{deviceMac}", method = RequestMethod.DELETE)
    public String deleteDevice(@PathVariable("deviceMac") String mac){
        deviceService.deleteDevice(mac);
        return "delete success";
    }

    @ApiOperation("Turn on device by mac address")
    @RequestMapping(value = "/turnOnDevice/{deviceMac}", method = RequestMethod.GET)
    public String turnOnDevice(@PathVariable("deviceMac") String mac){
        deviceService.turnOnDevice(mac);
        return "turn on success";
    }

    @ApiOperation("Turn off device by mac address")
    @RequestMapping(value = "/turnOffDevice/{deviceMac}", method = RequestMethod.GET)
    public String turnOffDevice(@PathVariable("deviceMac") String mac){
        deviceService.turnOffDevice(mac);
        return "turn off success";
    }

    @ApiOperation("Turn off sensor by mac address")
    @RequestMapping(value = "/turnOffSensor/{deviceMac}", method = RequestMethod.GET)
    public String turnOffSensor(@PathVariable("deviceMac") String mac){
        deviceService.turnOffDevice(mac);
        return "turn off sensor success";
    }


    @ApiOperation("Get Power Statistic Data for a device by mac address")
    @RequestMapping(value = "/getPowerData/{mac}", method = RequestMethod.GET)
    public List<PowerData> getPowerDataWithMac(@PathVariable("mac") String mac){
        List<PowerData> powerData =deviceService.getOrderedPowerDataByMac(mac);
        return powerData;
    }

    @ApiOperation("Get Power Statistic Data for a device by mac address during a period of time")
    @RequestMapping(value = "/getPowerDataDuring/{mac}/{startTime}/{endTime}", method = RequestMethod.GET)
    public List<PowerData> getPowerDataWithMacInRange(@PathVariable("mac") String mac,@PathVariable("startTime") Long startTime,@PathVariable("endTime") Long endTime){
        List<PowerData> powerData =deviceService.getOrderedPowerDataByMacInRange(mac, startTime, endTime);
        return powerData;
    }

    @ApiIgnore
    @RequestMapping(value = "/savePowerData/{mac}/{power}/{time}", method = RequestMethod.POST)
    public void savePowerData(@PathVariable("mac") String mac, @PathVariable("power") Double power,@PathVariable("time") Long time) {
        deviceService.setPowerData(mac, power, time);
    }


    @ApiOperation("Delete Power Statistic Data for a device by mac address")
    @RequestMapping(value = "/deletePowerData/{mac}", method = RequestMethod.DELETE)
    public void deletePowerData(@PathVariable("mac") String mac){
        deviceService.deletePowerData(mac);
    }

    @ApiOperation("Delete Power Statistic Data for a device before a specified time point")
    @RequestMapping(value = "/deleteExpiredPowerData/{mac}/{untilTime}", method = RequestMethod.DELETE)
    public void deletePowerData(@PathVariable("mac") String mac,@PathVariable("untilTime") Long time){
        deviceService.deleteExpiredPowerData(mac, time);
    }



}
