package com.example.demo.tasks;


import com.example.demo.entity.Device;
import com.example.demo.service.DeviceService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Component
public class ScheduledTask {

    @Autowired
    private DeviceService deviceService;

    @Scheduled(fixedRate = 86400000)
    public void scheduledTask() {
        List<Device> allDevice = deviceService.getAllDevice();
        Date now = new Date();
        Long expiredTime = DateUtils.addDays(now, -30).getTime();
        for(Device device: allDevice){
            deviceService.deleteExpiredPowerData(device.getDeviceMac(),expiredTime);
        }
    }

}
