package com.example.demo.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;


//package the jjwt for further use
public class JwtTokenUtils {

    public static final  String TOKEN_HEADER = "Authorization";
    public static final  String TOKEN_PREFIX = "Bearer ";

    //expiration period is 3600s
    private static final long EXPIRATION = 3600L;

    //if remember me is select, expiration period is 7 days
    private static final long EXPIRATION_REMEMBER = 604800L;
    private static final String SECRET = "smartenergyapp";
    private static final String ISS = "xuan";
    private static final String ROLE_CLAIMS = "rol";

    //create token
    public static String createToken(String username,String role,  boolean isRememberMe ){
        long expiration = isRememberMe ? EXPIRATION_REMEMBER : EXPIRATION;
        HashMap<String, Object> map = new HashMap<>();
        map.put(ROLE_CLAIMS, role);
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .setClaims(map)
                .setIssuer(ISS)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                .compact();
    }

    //get username from token
    public  static  String getUsername(String token){
        return getTokenBody(token).getSubject();
    }

    public static String getUserRole(String token){
        return (String) getTokenBody(token).get(ROLE_CLAIMS);
    }

    private static Claims getTokenBody(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
    }

    public static boolean isExpiration(String token) {
        try {
            return getTokenBody(token).getExpiration().before(new Date());
        } catch (ExpiredJwtException e) {
            return true;
        }
    }


}
