package com.example.demo.repository;

import com.example.demo.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface DeviceRepository  extends JpaRepository<Device, Integer> {

    @Transactional
    Device findByDeviceMac(String mac);

    @Transactional
    @Query("update Device set deviceName= ?2, power = ?3, heartTime = ?4, status =?5 where deviceMac = ?1 ")
    @Modifying(clearAutomatically = true)
    void updateDeviceInfoByMac(String mac,String deviceName, Double power, Long heartTime, Integer status);

    @Transactional
    @Query("update Device set status =?2 where deviceMac = ?1 ")
    @Modifying(clearAutomatically = true)
    void updateDeviceStatus(String mac, Integer status);

    @Transactional
    void deleteByDeviceMac(String mac);




}
