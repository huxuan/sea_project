package com.example.demo.repository;

import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface UserRepository extends CrudRepository<User, Integer> {

    @Transactional
    User findByUsername(String username);

    @Transactional
    @Query("update User set password = ?2 where username = ?1 ")
    @Modifying(clearAutomatically = true)
    void updatePasswordByUsername(String username, String password);

}
