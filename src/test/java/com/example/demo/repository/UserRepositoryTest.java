package com.example.demo.repository;


import com.example.demo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class
UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testFindByUsername() throws Exception {
        User newuser = new User();
        newuser.setUsername("lily");
        newuser.setPassword("123456");
        entityManager.persist(newuser);
        User user = repository.findByUsername("lily");
        assertThat(user.getPassword()).isEqualTo("123456");
    }

    @Test
    public void testUpdatePasswordById() throws Exception {
        User newuser = new User();
        newuser.setUsername("lily");
        newuser.setPassword("123456");
        entityManager.persist(newuser);
        repository.updatePasswordByUsername("lily","654321");
        User user = repository.findByUsername("lily");
        assertThat(user.getPassword()).isEqualTo("654321");
    }


}
