package com.example.demo.repository;


import com.example.demo.entity.Device;
import com.example.demo.entity.User;
import io.jsonwebtoken.lang.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeviceRepositoryTest {

    @Autowired
    private DeviceRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testFindByDeviceMac() throws Exception {
        Device newdevice = new Device();
        newdevice.setDeviceName("aaa");
        newdevice.setDeviceMac("12345678");
        newdevice.setPower(120.0);
        newdevice.setStatus(1);
        Date date = new Date();
        newdevice.setHeartTime(1432435L);
        entityManager.persist(newdevice);
        Device device = repository.findByDeviceMac("12345678");
        assertThat(device.getDeviceName()).isEqualTo("aaa");
        assertThat(device.getPower()).isEqualTo(120.0);
        assertThat(device.getStatus()).isEqualTo(1);
        assertThat(device.getHeartTime()).isEqualTo(1432435.3);
    }

    @Test
    public void testUpdateDeviceInfoByMac() throws Exception {
        Device newdevice = new Device();
        newdevice.setDeviceName("aaa");
        newdevice.setDeviceMac("12345678");
        newdevice.setPower(120.0);
        newdevice.setStatus(1);
        newdevice.setHeartTime(1432435L);
        entityManager.persist(newdevice);
        repository.updateDeviceInfoByMac("12345678","bbb", 150.0, 12593L, 2);
        Device device = repository.findByDeviceMac("12345678");
        assertThat(device.getDeviceName()).isEqualTo("bbb");
        assertThat(device.getPower()).isEqualTo(150.0);
        assertThat(device.getStatus()).isEqualTo(2);
        assertThat(device.getHeartTime()).isEqualTo(12593.2);
    }

    @Test
    public void testDeleteByDeviceMac() throws Exception {
        Device newdevice = new Device();
        newdevice.setDeviceName("aaa");
        newdevice.setDeviceMac("12345678");
        newdevice.setPower(120.0);
        newdevice.setStatus(1);
        newdevice.setHeartTime(1432435L);
        entityManager.persist(newdevice);
        repository.deleteByDeviceMac("12345678");
        Assert.isNull(repository.findByDeviceMac("12345678"));
    }



}
