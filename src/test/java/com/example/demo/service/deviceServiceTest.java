package com.example.demo.service;

import com.example.demo.model.PowerData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class deviceServiceTest {

    @Autowired
    DeviceService deviceService;


    @Test
    public void testRedisCache() {

        Date date = new Date();
        long time1 = date.getTime();
        long time2 = time1 + 1;
        long time3 = time2 + 1;
        long time4 = time3 + 1;
        deviceService.setPowerData("12345678", 120.0, time1);
        deviceService.setPowerData("12345678", 140.0, time2);
        deviceService.setPowerData("12345678", 130.0, time3);
        deviceService.setPowerData("12345678", 180.0, time4);
    }

    /**
     * 读取缓存数据
     */
    @Test
    public void testGetRedisCache() {
        Date date = new Date();
        long time = date.getTime();
        List<PowerData> powerDataList1 = deviceService.getOrderedPowerDataByMac("12345678");
        List<PowerData> powerDataList2 = deviceService.getOrderedPowerDataByMacInRange("12345678",0l,time);
        System.out.println("OrderedPowerDataByMac:" + powerDataList1);
        System.out.println("OrderedPowerDataByMacInRange:" + powerDataList2);
    }

    @Test
    public void testDeleteRedisCache() {
        Date date = new Date();
        long time = date.getTime()-10000;
        deviceService.deleteExpiredPowerData("12345678",time);
        System.out.println("OrderedPowerDataByMac after delete expired power data:" + deviceService.getOrderedPowerDataByMac("12345678"));
        deviceService.deletePowerData("12345678");
        System.out.println("OrderedPowerDataByMac after delete all:" + deviceService.getOrderedPowerDataByMac("12345678"));
    }




}
